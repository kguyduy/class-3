//You are given a string, a user input for their email, make sure it is in correct email format. Should be 1 or more
//characters, then @ sign, then 1 or more characters, then
//dot, then one or more characters - no whitespace:
//foo@bar.baz

function validateEmail(email) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  if (reg.test(email)) {
    return alert("Thanks!");
  } else {
    return alert("Please provide a valid email address");
  }
}

validateEmail("foo@bar.baz"); //Thanks!
