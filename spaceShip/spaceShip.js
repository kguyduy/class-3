// Create a constructor function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`

function SpaceShip(name, topSpeed) {
    this.name = name;
    let speed = topSpeed;
    this.accelerate = function() {
    	const {name, topSpeed} = this;
    	console.log(`${name} moving to ${speed}`);
    }
    this.changeSpeed = function(newTopSpeed) {
    	speed = newTopSpeed;
    }
};

const moonSpace = new SpaceShip('Moon Spaceship', 800);
const venusSpace = new SpaceShip('Venus Spaceship', 950);

console.log(moonSpace.accelerate()); //Moon Spaceship moving to 800
moonSpace.changeSpeed(1000); 
console.log(moonSpace.accelerate()); //Moon Spaceship moving to 1000

// Make name and topSpeed private



// Keep both name and topSpeed private, but 
// add a method that changes the topSpeed



// Call the constructor with a couple ships, change the topSpeed
// using the method, and call accelerate.
